terraform {
  backend "gcs" {
    bucket  = ""
    prefix  = ""
  }
}

variable "project" {
}

resource "google_storage_bucket" "test-bucket" {
  project = var.project
  name   = "test-bucket-${var.project}"
}
